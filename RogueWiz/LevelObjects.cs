﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using RogueWiz.lib;
using System;
using System.Collections.Generic;
using System.Linq;
using static Hate9.IntuitiveRNG;
using static RogueWiz.lib.GameVars;
using static RogueWiz.Methods;
using static RogueWiz.Program;

namespace RogueWiz
{
    public class Level : Scene
    {
        public Level(HashSet<GameComponent> components = null, Rectangle? camera = null, int songId = -1) : base(Color.Black, components, null, camera, songId) { }

        public override void SwitchTo()
        {
            game.health = 3;
            game.score = 0;
            game.spawnWait = 500;
            for (int x = 0; x < game.map.GetLength(0); x++)
            {
                for (int y = 0; y < game.map.GetLength(1); y++)
                {
                    game.map[x, y] = -1;
                }
            }
            components.RemoveWhere(x => x.ConvertibleTo<Goblin>());
            foreach (GameComponent component in components)
            {
                GameObject obj = component as GameObject;
                if (obj != null && !obj.ConvertibleTo<Bomb>())
                {
                    game.map[obj.mapLocation.X, obj.mapLocation.Y] = obj.id;
                }
            }
            base.SwitchTo();
        }
    }

    public delegate void GenericGameObjectEventHandler(ref CurrentGame game, GameTime gameTime, int id);
    /// <summary>Represents an animatable object (with a hitbox), to be placed within the game world.</summary>
    public class GameObject : DrawableGameComponent
    {
        public SpriteSet sprites;
        public Point currentSprite;
        public Rectangle bounds;
        public Point mapLocation;
        public int id;

        public Point Location
        {
            get => bounds.Location;
            set => bounds.Location = value;
        }

        public GameObject(SpriteSet sprites, Point mapLocation, Point? currentSprite = null, int drawOrder = 0) : base(game)
        {
            this.sprites = sprites;
            this.bounds = new Rectangle(mapLocation * sprites.spriteSize, sprites.spriteSize);
            this.mapLocation = mapLocation;
            this.currentSprite = currentSprite ?? Point.Zero;
            this.DrawOrder = drawOrder;

            id = objectIden.CreateId();
        }

        public override void Draw(GameTime gameTime)
        {
            game.DrawSprite(sprites[currentSprite.X, currentSprite.Y], bounds.Subtract(camera.Location));
        }

        public bool Collides(GameObject obj)
        {
            return bounds.Intersects(obj.bounds);
        }

        public int Move(Direction moveDirection)
        {
            return Move(RelativeLocation(moveDirection));
        }

        public Point RelativeLocation(Direction moveDirection)
        {
            Point movement = Point.Zero;
            switch (moveDirection)
            {
                case Direction.West:
                    movement.X = -1;
                    break;
                case Direction.North:
                    movement.Y = -1;
                    break;
                case Direction.East:
                    movement.X = 1;
                    break;
                case Direction.South:
                    movement.Y = 1;
                    break;
            }
            return mapLocation + movement;
        }

        public virtual int Move(Point newLocation)
        {
            if (newLocation.X >= game.map.GetLength(0) || newLocation.X < 0 ||
                newLocation.Y >= game.map.GetLength(1) || newLocation.Y < 0)
            {
                return id;
            }

            int returnId = game.map[newLocation.X, newLocation.Y];
            if (game.map[newLocation.X, newLocation.Y] == -1)
            {
                game.map[newLocation.X, newLocation.Y] = id;
                game.map[mapLocation.X, mapLocation.Y] = -1;
                mapLocation = newLocation;
            }
            return returnId;
        }
    }

    public delegate void GameObjectEventHandler(ref CurrentGame game, GameTime gameTime, int id);
    /// <summary>Represents an animatable object (with a hitbox), to be placed within the game world.</summary>
    public class GenericGameObject : GameObject
    {
        public Dictionary<string, object> vars;
        public GameObjectEventHandler updateEvent;
        public GameObjectEventHandler drawEvent;

        public GenericGameObject(SpriteSet sprites, Point mapLocation, Point? currentSprite = null, GameObjectEventHandler updateEvent = null, GameObjectEventHandler drawEvent = default, params (string, object)[] vars) :
            this(sprites, mapLocation, currentSprite, updateEvent, drawEvent, Array.ConvertAll(vars, x => new KeyValuePair<string, object>(x.Item1, x.Item2)))
        { }

        public GenericGameObject(SpriteSet sprites, Point mapLocation, Point? currentSprite = null, GameObjectEventHandler updateEvent = null, GameObjectEventHandler drawEvent = default, params KeyValuePair<string, object>[] vars) :
            this(sprites, mapLocation, currentSprite, updateEvent, drawEvent, vars == null ? new Dictionary<string, object>() : vars.ToDictionary(x => x.Key, x => x.Value))
        { }

        public GenericGameObject(SpriteSet sprites, Point mapLocation, Point? currentSprite = null, GameObjectEventHandler updateEvent = null, GameObjectEventHandler drawEvent = default, Dictionary<string, object> vars = null) : base(sprites, mapLocation, currentSprite)
        {
            this.updateEvent += updateEvent;
            this.drawEvent += drawEvent ?? BasicDraw;
            this.vars = vars;
        }

        public void BasicDraw(ref CurrentGame game, GameTime gameTime, int id)
        {
            base.Draw(gameTime);
        }

        public override void Update(GameTime gameTime)
        {
            updateEvent?.Invoke(ref game, gameTime, id);
        }

        public override void Draw(GameTime gameTime)
        {
            drawEvent?.Invoke(ref game, gameTime, id);
        }
    }

    public enum Direction
    {
        West,
        North,
        East,
        South
    }

    public class PlayerCharacter : GameObject
    {
        public int animationFrame,
            movementCooldown,
            hitCooldown;
        public Direction direction;
        public bool moving;

        public PlayerCharacter(SpriteSet sprites, Point mapLocation, Point? currentSprite = null) : base(sprites, mapLocation, currentSprite)
        {
            animationFrame = 0;
            movementCooldown = 0;
            hitCooldown = 0;
            direction = currentSprite == null ? Direction.South : (Direction)((Point)currentSprite).X;
            moving = false;
        }

        public override void Update(GameTime gameTime)
        {
            moving = true;
            if (keyboardState.IsKeyDown(Keys.A) || keyboardState.IsKeyDown(Keys.Left))
            {
                direction = Direction.West;
            }
            else if (keyboardState.IsKeyDown(Keys.W) || keyboardState.IsKeyDown(Keys.Up))
            {
                direction = Direction.North;
            }
            else if (keyboardState.IsKeyDown(Keys.D) || keyboardState.IsKeyDown(Keys.Right))
            {
                direction = Direction.East;
            }
            else if (keyboardState.IsKeyDown(Keys.S) || keyboardState.IsKeyDown(Keys.Down))
            {
                direction = Direction.South;
            }
            else
            {
                moving = false;
            }

            currentSprite.X = (int)direction;

            if (movementCooldown == 0)
            {
                if (keyboardState.IsKeyDown(Keys.Space) || keyboardState.IsKeyDown(Keys.Z))
                {
                    if (game.Components.FirstOrDefault(x => x.ConvertibleTo<Bomb>() && ((Bomb)x).mapLocation == mapLocation) == null)
                    {
                        game.Components.Add(new Bomb(mapLocation));
                        effects["cast"].Play();

                        movementCooldown = 20;
                    }
                }
                else if (moving)
                {
                    Point movement = Point.Zero;
                    switch (direction)
                    {
                        case Direction.West:
                            movement.X = -1;
                            break;
                        case Direction.North:
                            movement.Y = -1;
                            break;
                        case Direction.East:
                            movement.X = 1;
                            break;
                        case Direction.South:
                            movement.Y = 1;
                            break;
                    }

                    currentSprite.X = (int)direction;

                    Point loc = mapLocation;
                    Point newLoc = mapLocation + movement;
                    int moveResult = Move(newLoc);
                    if (moveResult != -1)
                    {
                        if (game.Components.FirstOrDefault(x => x.ConvertibleTo<Goblin>() && ((Goblin)x).mapLocation == newLoc) != null)
                        {
                            TakeDamage();
                        }
                        if (mapLocation != loc)
                            Move(loc);
                    }
                    else
                    {
                        Bomb underFoot = (Bomb)game.Components.FirstOrDefault(x => x.ConvertibleTo<Bomb>() && ((Bomb)x).mapLocation == mapLocation);
                        if (underFoot != null)
                        {
                            game.Components.Remove(underFoot);
                            TakeDamage();
                        }

                        Location += movement * new Point(4);
                        DelayCast(x => Location += movement * new Point(4), 0.1);
                        effects["step"].Play();
                    }

                    movementCooldown = 20;
                }
            }
            else
            {
                movementCooldown--;
            }

            if (hitCooldown > 0)
            {
                hitCooldown--;
            }
        }

        public void TakeDamage()
        {
            RandomCameraShake(randomMultiplier: 1, distance: 2);
            effects["hit"].Play();
            if (hitCooldown == 0)
            {
                game.health--;
                hitCooldown = 40;
                if (game.health == 0)
                {
                    Die();
                }
            }
        }

        public void Die()
        {
            effects["die"].Play();
            scenes.point = 1;
        }

        public override void Draw(GameTime gameTime)
        {
            if (moving)
            {
                if (animationFrame >= 10)
                {
                    currentSprite.Y++;
                    if (currentSprite.Y >= sprites.Height)
                        currentSprite.Y = 0;

                    animationFrame = 0;
                }
                animationFrame++;
            }
            else
            {
                animationFrame = 0;
                currentSprite.Y = 0;
            }

            base.Draw(gameTime);
        }
    }

    public class Bomb : GameObject
    {
        public int animationFrame;

        public Bomb(Point mapLocation) : base(new SpriteSet(game.Content.Load<Texture2D>("bomb")), mapLocation, Point.Zero, -2)
        {
            animationFrame = 0;
        }

        public override void Draw(GameTime gameTime)
        {
            animationFrame++;
            if (animationFrame == 15)
            {
                currentSprite.Y++;
                if (currentSprite.Y >= sprites.Height)
                {
                    currentSprite.Y = 0;
                }
                animationFrame = 0;
            }
            base.Draw(gameTime);
        }
    }

    public class Goblin : GameObject
    {
        public int animationFrame,
            movementCooldown;

        public Goblin(Point mapLocation, Point? currentSprite = null) : base(new SpriteSet(game.Content.Load<Texture2D>("goblin")), mapLocation, currentSprite, -1)
        {
            animationFrame = 0;
            movementCooldown = 100;
        }

        public void Kill()
        {
            game.score++;
            for (int x = 0; x < game.map.GetLength(0); x++)
            {
                for (int y = 0; y < game.map.GetLength(1); y++)
                {
                    if (game.map[x, y] == id)
                    {
                        game.map[x, y] = -1;
                    }
                }
            }
            effects["kill"].Play();
            game.Components.Remove(this);
        }

        public override void Update(GameTime gameTime)
        {
            PlayerCharacter player = (PlayerCharacter)game.Components.FirstOrDefault(x => x.ConvertibleTo<PlayerCharacter>());
            if (movementCooldown == 0 && player != null)
            {
                Point playerLocation = player.mapLocation;
                Point relativePlayerLocation = playerLocation - mapLocation;

                List<Direction> directions = new List<Direction>(2);

                if (relativePlayerLocation.X > 0)
                {
                    directions.Add(Direction.East);
                }
                else if (relativePlayerLocation.X < 0)
                {
                    directions.Add(Direction.West);
                }

                if (relativePlayerLocation.Y > 0)
                {
                    directions.Add(Direction.South);
                }
                else if (relativePlayerLocation.Y < 0)
                {
                    directions.Add(Direction.North);
                }

                if (directions.Count > 1 && realRNG.Next(2) == 1)
                {
                    (directions[0], directions[1]) = (directions[1], directions[0]);
                }

                while (directions.Count >= 1)
                {
                    Point newLocation = RelativeLocation(directions[0]);

                    currentSprite.X = (int)directions[0];

                    Point loc = mapLocation;
                    int moveResult = Move(newLocation);
                    if (moveResult != -1)
                    {
                        if (newLocation == playerLocation)
                        {
                            player.TakeDamage();
                        }
                        else
                        {
                            directions.RemoveAt(0);
                            continue;
                        }
                        if (mapLocation != loc)
                            Move(loc);
                    }
                    else
                    {
                        Location = newLocation * new Point(8);
                        Bomb underFoot = (Bomb)game.Components.FirstOrDefault(x => x.ConvertibleTo<Bomb>() && ((Bomb)x).mapLocation == newLocation);
                        if (underFoot != null)
                        {
                            game.Components.Remove(underFoot);
                            Kill();
                        }
                    }

                    movementCooldown = 100;
                    directions.Clear();
                }
            }
            else
            {
                movementCooldown--;
            }
        }

        public override void Draw(GameTime gameTime)
        {
            if (animationFrame == 20)
            {
                currentSprite.Y++;
                if (currentSprite.Y >= sprites.Height)
                    currentSprite.Y = 0;

                animationFrame = 0;
            }
            animationFrame++;

            base.Draw(gameTime);
        }
    }
}
