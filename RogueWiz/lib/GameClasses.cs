﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Media;
using System;
using System.Collections;
using System.Collections.Generic;
using static RogueWiz.lib.GameVars;
using static RogueWiz.Program;

namespace RogueWiz.lib
{
    /// <summary>A representation of a List, with a point representing the active entry.</summary>
    public class ListP<T> : List<T>
    {
        public delegate void EventHandler(int point);
        public event EventHandler OnPointChanged;
        private int _point = 0;
        /// <summary>Represents the active entry point</summary>
        public int point
        {
            get { return _point; }
            set
            {
                _point = value;
                if (point >= 0)
                {
                    OnPointChanged?.Invoke(_point);
                }
            }
        }

        public T GetValueAtPoint()
        {
            return this[point];
        }

        new public int Add(T item)
        {
            base.Add(item);
            return Count;
        }
    }

    /// <summary>Represents a countdown timer.</summary>
    public class Timer : GameComponent
    {
        public delegate void TimerEventHandler(params object[] args);
        public double waitTime, currentTime = 0;
        public bool repeat, remove;
        public event TimerEventHandler TimerTickEvent;
        public object[] args;
        private bool enabled = true;

        /// <param name="waitTime">How many seconds the timer waits before each tick.</param>
        /// <param name="repeat"></param>
        /// <param name="remove"></param>
        /// <param name="timerTick"></param>
        /// <param name="args"></param>
        public Timer(double waitTime = 0, bool repeat = false, bool remove = false, TimerEventHandler timerTick = null, params object[] args) : base(game)
        {
            this.waitTime = waitTime;
            this.repeat = repeat;
            this.remove = remove;
            TimerTickEvent = timerTick;
            this.args = args;
        }

        public override void Update(GameTime gameTime)
        {
            if (enabled)
            {
                currentTime += gameTime.ElapsedGameTime.TotalSeconds;
                if (currentTime >= waitTime - gameTime.ElapsedGameTime.TotalSeconds)
                {
                    TimerTickEvent?.Invoke(args);
                    if (repeat)
                    {
                        currentTime = 0;
                    }
                    else
                    {
                        enabled.Unset();
                        if (remove)
                        {
                            game.Components.Remove(this);
                        }
                    }
                }
            }
        }
    }

    /// <summary>
    /// An event handler for <see cref="FunctionComponent"/>s and <see cref="FunctionDrawComponent"/>s.
    /// </summary>
    /// <param name="game">The game currently running.</param>
    /// <param name="gameTime">Provides a snapshot of timing values</param>
    /// <param name="vars">The available dictionary of objects for inter-tick information storage</param>
    public delegate void ComponentEventHandler(ref CurrentGame game, GameTime gameTime, ref Dictionary<string, object> vars);
    /// <summary>
    /// A simple generic <see cref="GameComponent"/> which runs a specified <see cref="ComponentEventHandler"/> every update cycle, with access to some local storage via a dictionary of objects.
    /// </summary>
    public class FunctionComponent : GameComponent
    {
        public ComponentEventHandler ComponentEvent;
        public Dictionary<string, object> vars;

        public FunctionComponent(ComponentEventHandler componentEvent, Dictionary<string, object> vars = null) : base(game)
        {
            ComponentEvent = componentEvent;
            this.vars = vars ?? new Dictionary<string, object>();
        }

        public override void Update(GameTime gameTime)
        {
            ComponentEvent?.Invoke(ref game, gameTime, ref vars);
        }
    }

    /// <summary>
    /// A simple generic <see cref="GameComponent"/> which runs a specified <see cref="ComponentEventHandler"/> every update cycle, and another every draw cycle.
    /// </summary>
    public class FunctionDrawComponent : DrawableGameComponent
    {
        public ComponentEventHandler ComponentEvent,
            ComponentDrawEvent;
        public Dictionary<string, object> vars;

        public FunctionDrawComponent(ComponentEventHandler componentEvent = null, ComponentEventHandler componentDrawEvent = null, Dictionary<string, object> vars = null, int drawOrder = 0) : base(game)
        {
            ComponentEvent = componentEvent;
            ComponentDrawEvent = componentDrawEvent;
            this.vars = vars ?? new Dictionary<string, object>();
            this.DrawOrder = drawOrder;
        }

        public override void Update(GameTime gameTime)
        {
            ComponentEvent?.Invoke(ref game, gameTime, ref vars);
        }

        public override void Draw(GameTime gameTime)
        {
            ComponentDrawEvent?.Invoke(ref game, gameTime, ref vars);
        }
    }

    /// <summary>
    /// A simple <see cref="GameComponent"/> which draws a <see cref="Texture2D"/> at a <see cref="Point"/> with an optional draw order.
    /// </summary>
    public class ImageDrawComponent : DrawableGameComponent
    {
        public Texture2D image;
        public Point location;

        public ImageDrawComponent(Texture2D image, Point location, int drawOrder = 0) : base(game)
        {
            this.image = image;
            this.location = location;
            this.DrawOrder = drawOrder;
        }

        public override void Draw(GameTime gameTime)
        {
            game.DrawSprite(image, location);
        }
    }

    /// <summary>
    /// Represents a sprite sheet with a multidimensional array of <see cref="Texture2D"/>s.
    /// </summary>
    public class SpriteSet : ICollection
    {
        public static Point DefaultSpriteSize => new Point(8);
        public Point spriteSize;

        private Texture2D[,] sprites;

        public Texture2D this[int index1, int index2]
        {
            get
            {
                return sprites[index1, index2];
            }
            set
            {
                sprites[index1, index2] = value;
            }
        }

        public SpriteSet(Texture2D source) : this(source, DefaultSpriteSize) { }

        public SpriteSet(Texture2D source, Point spriteSize)
        {
            if (source.Width % spriteSize.X != 0 ||
                source.Height % spriteSize.Y != 0)
            {
                throw new ArgumentException($"Texture dimensions are not divisible by sprite size ({spriteSize}).", "source");
            }

            this.spriteSize = spriteSize;
            sprites = GetSprites(source, spriteSize);
        }

        public bool IsReadOnly => true;

        public bool IsFixedSize => true;

        public int Count => sprites.Length;

        /// <summary>The number of columns in the sheet.</summary>
        public int Width => sprites.GetLength(0);

        /// <summary>The number of rows in the sheet.</summary>
        public int Height => sprites.GetLength(1);

        public object SyncRoot => sprites.SyncRoot;

        public bool IsSynchronized => false;

        /// <summary>
        /// Constructs a multidimensional array of <see cref="Texture2D"/>s from a single source image
        /// </summary>
        /// <param name="source">The unprocessed spritesheet</param>
        /// <param name="spriteSize">The size of each cell of the spritesheet</param>
        private static Texture2D[,] GetSprites(Texture2D source, Point? spriteSize = null)
        {
            Point realSpriteSize = spriteSize ?? DefaultSpriteSize;
            if (source.Width % realSpriteSize.X != 0 ||
                source.Height % realSpriteSize.Y != 0)
                return null;

            Texture2D[,] returnValue = new Texture2D[source.Width / realSpriteSize.X, source.Height / realSpriteSize.Y];
            for (int x = 0, xmax = returnValue.GetLength(0); x < xmax; x++)
            {
                for (int y = 0, ymax = returnValue.GetLength(1); y < ymax; y++)
                {
                    returnValue[x, y] = GetSprite(source, x, y, realSpriteSize);
                }
            }
            return returnValue;
        }

        /// <summary>
        /// Gets a single sprite from an unprocessed spritesheet
        /// </summary>
        /// <param name="source">The unprocessed spritesheet</param>
        /// <param name="x">The sprite's horizontal location, in sprites</param>
        /// <param name="y">The sprite's vertical location, in sprites</param>
        /// <param name="spriteSize">The size of each sprite in the sheet</param>
        private static Texture2D GetSprite(Texture2D source, int x, int y, Point? spriteSize = null)
        {
            return GetSprite(source, new Point(x, y), spriteSize);
        }

        /// <summary>
        /// Gets a single sprite from an unprocessed spritesheet
        /// </summary>
        /// <param name="source">The unprocessed spritesheet</param>
        /// <param name="spriteLocation">The sprite's location, in sprites</param>
        /// <param name="spriteSize">The size of each sprite in the sheet</param>
        private static Texture2D GetSprite(Texture2D source, Point spriteLocation, Point? spriteSize = null)
        {
            Point realSpriteSize = spriteSize ?? DefaultSpriteSize;
            Rectangle sourceRect = new Rectangle(realSpriteSize.X * spriteLocation.X, realSpriteSize.Y * spriteLocation.Y, realSpriteSize.X, realSpriteSize.Y);

            Texture2D sprite = new Texture2D(game.GraphicsDevice, sourceRect.Width, sourceRect.Height);
            Color[] colorData = new Color[sourceRect.Width * sourceRect.Height];

            source.GetData(0, sourceRect, colorData, 0, colorData.Length);
            sprite.SetData(colorData);

            return sprite;
        }

        /// <summary>
        /// Changes the number of rows and/or columns in the spritesheet. Cannot make the sheet smaller, only larger.
        /// </summary>
        /// <param name="x">The number of columns</param>
        /// <param name="y">The number of rows</param>
        public bool Resize(int x, int y)
        {
            return Resize(new Point(x, y));
        }

        /// <summary>
        /// Changes the number of rows and/or columns in the spritesheet. Cannot make the sheet smaller, only larger.
        /// </summary>
        /// <param name="size">The new size</param>
        public bool Resize(Point size)
        {
            Point temp = new Point(Width, Height);
            sprites = Arrays.ResizeArray(sprites, size.X, size.Y);
            return temp.X != Width ||
                temp.Y != Height;
        }

        /// <summary>
        /// Adds a row to the spritsheet
        /// </summary>
        /// <param name="source">The unprocessed source for the addition</param>
        public void AddRow(Texture2D source)
        {
            if (source.Width != Width * spriteSize.X ||
                source.Height != spriteSize.Y)
                throw new ArgumentException($"Texture dimensions must be equal to {Width * spriteSize.X}x{spriteSize.Y}.", "source");

            AddRow(GetSprites(source).GetRow(0));
        }

        /// <summary>
        /// Adds a row to the spritesheet
        /// </summary>
        /// <param name="source">The processed source for the addition</param>
        public void AddRow(Texture2D[] source)
        {
            Resize(Width, Height + 1);
            for (int i = 0; i < Width; i++)
            {
                sprites[i, Height - 1] = source[i];
            }
        }

        /// <summary>
        /// Adds a column to the spritesheet
        /// </summary>
        /// <param name="source">The unprocessed source for the addition</param>
        public void AddCol(Texture2D source)
        {
            if (source.Width != spriteSize.X ||
                source.Height != Height * spriteSize.Y)
                throw new ArgumentException($"Texture dimensions must be equal to {spriteSize.X}x{Height * spriteSize.Y}.", "source");

            AddCol(GetSprites(source).GetCol(0));
        }

        /// <summary>
        /// Adds a column to the spritesheet
        /// </summary>
        /// <param name="source">The processed source for the addition</param>
        public void AddCol(Texture2D[] source)
        {
            Resize(Width + 1, Height);
            for (int i = 0; i < Height; i++)
            {
                sprites[Width - 1, i] = source[i];
            }
        }

        public bool Contains(object value)
        {
            foreach (Texture2D sprite in sprites)
            {
                if (sprite == value)
                    return true;
            }
            return false;
        }

        public void CopyTo(Array array, int index)
        {
            foreach (Texture2D sprite in sprites)
            {
                array.SetValue(sprite, index);
                index++;
            }
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return new Enumerator(sprites);
        }

        private class Enumerator : IEnumerator
        {
            private Texture2D[,] array;
            private int Cursor;

            public Enumerator(Texture2D[,] array)
            {
                this.array = array;
                Cursor = -1;
            }

            object IEnumerator.Current
            {
                get
                {
                    if (Cursor < 0 || Cursor == array.Length)
                        throw new InvalidOperationException();

                    int x = Cursor % array.GetLength(0),
                        y = Cursor / array.GetLength(0);
                    return array[x, y];
                }
            }

            bool IEnumerator.MoveNext()
            {
                if (Cursor < array.Length)
                    Cursor++;

                return Cursor != array.Length;
            }

            void IEnumerator.Reset()
            {
                Cursor = -1;
            }
        }
    }

    /// <summary>
    /// Represents a scene within the game, with a background color, components, window dimensions, and camera
    /// </summary>
    public class Scene
    {
        public Color backgroundColor;
        public readonly HashSet<GameComponent> components;
        public Point screenDimensions;
        public Rectangle? camera;
        /// <summary>Represents the id in <see cref="music"/> which will be played during this scene. If the value is equal to -1, no song will be played.</summary>
        public int songId;

        public Scene(Color backgroundColor = default, HashSet<GameComponent> components = null, Point? screenDimensions = null, Rectangle? camera = null, int songId = -1)
        {
            this.backgroundColor = backgroundColor;
            this.components = components ?? new HashSet<GameComponent>();
            this.screenDimensions = screenDimensions ?? new Point(Properties.Settings.Default.Dimensions.X, Properties.Settings.Default.Dimensions.Y);
            this.camera = camera;
            this.songId = songId;
        }

        /// <summary>
        /// Switches this scene for the game's existing one
        /// </summary>
        public virtual void SwitchTo()
        {
            Point dimensions = screenDimensions;
            if (Properties.Settings.Default.Fullscreen && GraphicsAdapter.DefaultAdapter.CurrentDisplayMode.Width / (float)GraphicsAdapter.DefaultAdapter.CurrentDisplayMode.Height == dimensions.X / (float)dimensions.Y)
            {
                graphics.PreferredBackBufferWidth = GraphicsAdapter.DefaultAdapter.CurrentDisplayMode.Width;
                graphics.PreferredBackBufferHeight = GraphicsAdapter.DefaultAdapter.CurrentDisplayMode.Height;
                graphics.IsFullScreen = true;
            }
            else
            {
                graphics.PreferredBackBufferWidth = dimensions.X;
                graphics.PreferredBackBufferHeight = dimensions.Y;
                graphics.IsFullScreen = false;
            }

            game.Components.Clear();
            foreach (GameComponent component in components)
            {
                game.Components.Add(component);
            }
            if (camera == null)
            {
                GameVars.camera.Size = dimensions;
            }
            else
            {
                GameVars.camera = (Rectangle)camera;
            }
            MediaPlayer.Stop();
            Methods.PlaySong(songId);

            preRenderTarget = new RenderTarget2D(game.GraphicsDevice, GameVars.camera.Width, GameVars.camera.Height);
            graphics.ApplyChanges();
        }
    }
}
