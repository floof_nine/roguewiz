﻿using Hate9;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using System;
using System.Collections.Generic;

namespace RogueWiz.lib
{
    static class GameVars
    {
        /// <summary>The graphics device manager for the game.</summary>
        public static GraphicsDeviceManager graphics;
        /// <summary>The RenderTarget to be rendered to before scaling is applied.</summary>
        public static RenderTarget2D preRenderTarget;
        public static SpriteBatch spriteBatch;

        /// <summary>The Identification object to be used for GameObjects.</summary>
        public static Identification objectIden;

        public static Dictionary<int, object> variables;

        /// <summary>A public list of songs, available to the entire program.</summary>
        public static ListP<Song> music;
        /// <summary>A public list of sound effects, available to the entire program.</summary>
        public static Dictionary<string, SoundEffect> effects;

        /// <summary>The List of all sprites for the cursor.</summary>
        public static ListP<Texture2D> cursor;
        /// <summary>Determines whether or not the cursor is visible.</summary>
        public static Boolean showCursor;

        /// <summary>The List of all Levels in the game.</summary>
        public static ListP<Scene> scenes;

        /// <summary>A large number of Boolean flags.</summary>
        public static Boolean[] flags;

        /// <summary>The current cursor location and button press information of the mouse.</summary>
        public static MouseState mouseState;
        /// <summary>The Point representation of the cursor location.</summary>
        public static Point mousePosition;

        /// <summary>The current button press information for the keyboard.</summary>
        public static KeyboardState keyboardState;

        /// <summary>A Rectangle representing the current location and size of the (2-dimensional) game camera.</summary>
        public static Rectangle camera;
    }
}
