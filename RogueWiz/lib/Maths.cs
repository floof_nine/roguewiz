﻿using Microsoft.Xna.Framework;
using System;
using System.Linq;
using static Hate9.IntuitiveRNG;

namespace RogueWiz.lib
{
    static class Maths
    {
        public const double TAU = Math.PI * 2;

        public static double RandomRadian()
        {
            return randomNumber * Maths.TAU;
        }

        public static double AddFirstN(double[] values, int n)
        {
            return AddAll(values.Take(n).ToArray());
        }

        public static double AddAll(params double[] doubles)
        {
            double temp = 0;
            foreach (double d in doubles)
            {
                temp += d;
            }
            return temp;
        }

        public static Point RectangleCenter(this Rectangle rectangle)
        {
            return rectangle.Location + rectangle.Size / new Point(2);
        }

        public static Rectangle Add(this Rectangle left, Point right)
        {
            return new Rectangle(left.X + right.X, left.Y + right.Y, left.Width, left.Height);
        }

        public static Rectangle Subtract(this Rectangle left, Point right)
        {
            return new Rectangle(left.X - right.X, left.Y - right.Y, left.Width, left.Height);
        }

        private static Point WeightedAveragePoint(Point a, Point b, decimal weight)
        {
            return new Point(
                WeightedAverage(a.X, b.X, weight),
                WeightedAverage(a.Y, b.Y, weight)
            );
        }

        private static int WeightedAverage(int a, int b, decimal weight)
        {
            return (int)(
                (1 - weight) * a +
                weight * b
            );
        }

        public static bool IsNegative(this Point point)
        {
            return point.X < 0 || point.Y < 0;
        }
    }
}
