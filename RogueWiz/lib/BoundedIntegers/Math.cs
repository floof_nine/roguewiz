﻿using System;

namespace Hate9.BoundedIntegers
{
    public class BInt16
    {
        public delegate void EventHandler(Int16 offset);
        public event EventHandler BoundsExceeded;

        private readonly Int16 value = 0;

        private BInt16(Int16 value, Action<Int16> method = null)
        {
            this.value = value;
            if (method != null)
            {
                BoundsExceeded += new EventHandler(method);
            }
        }

        public static implicit operator Int16(BInt16 value)
        {
            return value.value;
        }

        public static implicit operator BInt16(Int16 value)
        {
            return new BInt16(value);
        }

        public static BInt16 operator -(BInt16 left, BInt16 right)
        {
            if (right < 0)
            {
                return left + (BInt16)(0 - (Int16)right);
            }
            if (((Int16)left - (Int16)right) > left)
            {
                left.BoundsExceeded((Int16)((Int16)right - (Int16)left));
                return Int16.MinValue;
            }
            return (BInt16)((Int16)left - (Int16)right);
        }

        public static BInt16 operator +(BInt16 left, BInt16 right)
        {
            if (right < 0)
            {
                return left - (BInt16)(0 - (Int16)right);
            }
            if (((Int16)left + (Int16)right) < left)
            {
                left.BoundsExceeded((Int16)((Int16)right - (Int16)left));
                return Int16.MinValue;
            }
            return (BInt16)((Int16)left - (Int16)right);
        }
    }

    public class BUInt16
    {
        public delegate void EventHandler(Int16 offset);
        public event EventHandler BoundsExceeded;

        private readonly UInt16 value = 0;

        private BUInt16(UInt16 value, Action<Int16> method = null)
        {
            this.value = value;
            if (method != null)
            {
                BoundsExceeded += new EventHandler(method);
            }
        }

        public static implicit operator UInt16(BUInt16 value)
        {
            return value.value;
        }

        public static implicit operator BUInt16(UInt16 value)
        {
            return new BUInt16(value);
        }

        public static BUInt16 operator -(BUInt16 left, BUInt16 right)
        {
            if (right < 0)
            {
                return left + (BUInt16)(0 - (UInt16)right);
            }
            if (((UInt16)left - (UInt16)right) > left)
            {
                left.BoundsExceeded((Int16)((UInt16)right - (UInt16)left));
                return UInt16.MinValue;
            }
            return (BUInt16)((UInt16)left - (UInt16)right);
        }

        public static BUInt16 operator +(BUInt16 left, BUInt16 right)
        {
            if (right < 0)
            {
                return left - (BUInt16)(0 - (UInt16)right);
            }
            if (((UInt16)left + (UInt16)right) < left)
            {
                left.BoundsExceeded((Int16)((UInt16)right - (UInt16)left));
                return UInt16.MinValue;
            }
            return (BUInt16)((UInt16)left - (UInt16)right);
        }
    }

    public class BInt32
    {
        public delegate void EventHandler(Int32 offset);
        public event EventHandler BoundsExceeded;

        private readonly Int32 value = 0;

        private BInt32(Int32 value, Action<Int32> method = null)
        {
            this.value = value;
            if (method != null)
            {
                BoundsExceeded += new EventHandler(method);
            }
        }

        public static implicit operator Int32(BInt32 value)
        {
            return value.value;
        }

        public static implicit operator BInt32(Int32 value)
        {
            return new BInt32(value);
        }

        public static BInt32 operator -(BInt32 left, BInt32 right)
        {
            if (right < 0)
            {
                return left + (BInt32)(0 - (Int32)right);
            }
            if (((Int32)left - (Int32)right) > left)
            {
                left.BoundsExceeded((Int32)((Int32)right - (Int32)left));
                return Int32.MinValue;
            }
            return (BInt32)((Int32)left - (Int32)right);
        }

        public static BInt32 operator +(BInt32 left, BInt32 right)
        {
            if (right < 0)
            {
                return left - (BInt32)(0 - (Int32)right);
            }
            if (((Int32)left + (Int32)right) < left)
            {
                left.BoundsExceeded((Int32)((Int32)right - (Int32)left));
                return Int32.MinValue;
            }
            return (BInt32)((Int32)left - (Int32)right);
        }
    }

    public class BUInt32
    {
        public delegate void EventHandler(Int32 offset);
        public event EventHandler BoundsExceeded;

        private readonly UInt32 value = 0;

        private BUInt32(UInt32 value, Action<Int32> method = null)
        {
            this.value = value;
            if (method != null)
            {
                BoundsExceeded += new EventHandler(method);
            }
        }

        public static implicit operator UInt32(BUInt32 value)
        {
            return value.value;
        }

        public static implicit operator BUInt32(UInt32 value)
        {
            return new BUInt32(value);
        }

        public static BUInt32 operator -(BUInt32 left, BUInt32 right)
        {
            if (right < 0)
            {
                return left + (BUInt32)(0 - (UInt32)right);
            }
            if (((UInt32)left - (UInt32)right) > left)
            {
                left.BoundsExceeded((Int32)((UInt32)right - (UInt32)left));
                return UInt32.MinValue;
            }
            return (BUInt32)((UInt32)left - (UInt32)right);
        }

        public static BUInt32 operator +(BUInt32 left, BUInt32 right)
        {
            if (right < 0)
            {
                return left - (BUInt32)(0 - (UInt32)right);
            }
            if (((UInt32)left + (UInt32)right) < left)
            {
                left.BoundsExceeded((Int32)((UInt32)right - (UInt32)left));
                return UInt32.MinValue;
            }
            return (BUInt32)((UInt32)left - (UInt32)right);
        }
    }

    public class BInt64
    {
        public delegate void EventHandler(Int64 offset);
        public event EventHandler BoundsExceeded;

        private readonly Int64 value = 0;

        private BInt64(Int64 value, Action<Int64> method = null)
        {
            this.value = value;
            if (method != null)
            {
                BoundsExceeded += new EventHandler(method);
            }
        }

        public static implicit operator Int64(BInt64 value)
        {
            return value.value;
        }

        public static implicit operator BInt64(Int64 value)
        {
            return new BInt64(value);
        }

        public static BInt64 operator -(BInt64 left, BInt64 right)
        {
            if (right < 0)
            {
                return left + (BInt64)(0 - (Int64)right);
            }
            if (((Int64)left - (Int64)right) > left)
            {
                left.BoundsExceeded((Int64)((Int64)right - (Int64)left));
                return Int64.MinValue;
            }
            return (BInt64)((Int64)left - (Int64)right);
        }

        public static BInt64 operator +(BInt64 left, BInt64 right)
        {
            if (right < 0)
            {
                return left - (BInt64)(0 - (Int64)right);
            }
            if (((Int64)left + (Int64)right) < left)
            {
                left.BoundsExceeded((Int64)((Int64)right - (Int64)left));
                return Int64.MinValue;
            }
            return (BInt64)((Int64)left - (Int64)right);
        }
    }

    public class BUInt64
    {
        public delegate void EventHandler(Int64 offset);
        public event EventHandler BoundsExceeded;

        private readonly UInt64 value = 0;

        private BUInt64(UInt64 value, Action<Int64> method = null)
        {
            this.value = value;
            if (method != null)
            {
                BoundsExceeded += new EventHandler(method);
            }
        }

        public static implicit operator UInt64(BUInt64 value)
        {
            return value.value;
        }

        public static implicit operator BUInt64(UInt64 value)
        {
            return new BUInt64(value);
        }

        public static BUInt64 operator -(BUInt64 left, BUInt64 right)
        {
            if (right < 0)
            {
                return left + (BUInt64)(0 - (UInt64)right);
            }
            if (((UInt64)left - (UInt64)right) > left)
            {
                left.BoundsExceeded((Int64)((UInt64)right - (UInt64)left));
                return UInt64.MinValue;
            }
            return (BUInt64)((UInt64)left - (UInt64)right);
        }

        public static BUInt64 operator +(BUInt64 left, BUInt64 right)
        {
            if (right < 0)
            {
                return left - (BUInt64)(0 - (UInt64)right);
            }
            if (((UInt64)left + (UInt64)right) < left)
            {
                left.BoundsExceeded((Int64)((UInt64)right - (UInt64)left));
                return UInt64.MinValue;
            }
            return (BUInt64)((UInt64)left - (UInt64)right);
        }
    }
}
