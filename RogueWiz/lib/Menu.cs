﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using static RogueWiz.lib.GameVars;
using static RogueWiz.Program;

namespace RogueWiz.lib
{
    /// <summary>Represents a clickable <see cref="Menu"/> button, with methods to be run when it is clicked.</summary>
    public class Button : GameObject
    {
        /// <summary>
        /// Represents a button event handler
        /// </summary>
        /// <param name="game">The main game class</param>
        /// <param name="gameTime">Provides a snapshot of timing values</param>
        /// <param name="id">The id of the button which has been pressed.</param>
        /// <param name="state">Represents the state of the mouse on the button, either pressing the button or releasing it.</param>
        public delegate void ButtonEventHandler(ref CurrentGame game, GameTime gameTime, int id, ButtonState state);

        public ButtonEventHandler hoverEvent;
        public ButtonEventHandler rightEvent;
        public ButtonEventHandler leftEvent;
        public ButtonEventHandler middleEvent;
        bool rightDown = false, leftDown = false, middleDown = false;

        public Button(SpriteSet sprites, Rectangle bounds,
            ButtonEventHandler hoverEvent = null, ButtonEventHandler rightEvent = null, ButtonEventHandler leftEvent = null, ButtonEventHandler middleEvent = null) :
            base(sprites, new Point(0))
        {
            this.bounds = bounds;
            this.hoverEvent = hoverEvent;
            this.rightEvent = rightEvent;
            this.leftEvent = leftEvent;
            this.middleEvent = middleEvent;
        }

        public override void Update(GameTime gameTime)
        {
            if (bounds.Contains(mousePosition))
            {
                hoverEvent?.Invoke(ref game, gameTime, id, ButtonState.Pressed);
                if (mouseState.RightButton == ButtonState.Pressed)
                {
                    if (!rightDown)
                    {
                        rightEvent?.Invoke(ref game, gameTime, id, ButtonState.Pressed);
                    }
                    rightDown.Set();
                }
                else
                {
                    if (rightDown)
                    {
                        rightEvent?.Invoke(ref game, gameTime, id, ButtonState.Released);
                    }
                    rightDown.Unset();
                }

                if (mouseState.LeftButton == ButtonState.Pressed)
                {
                    if (!leftDown)
                    {
                        leftEvent?.Invoke(ref game, gameTime, id, ButtonState.Pressed);
                    }
                    leftDown.Set();
                }
                else
                {
                    if (leftDown)
                    {
                        leftEvent?.Invoke(ref game, gameTime, id, ButtonState.Released);
                    }
                    leftDown.Unset();
                }

                if (mouseState.MiddleButton == ButtonState.Pressed)
                {
                    if (!middleDown)
                    {
                        middleEvent?.Invoke(ref game, gameTime, id, ButtonState.Pressed);
                    }
                    middleDown.Set();
                }
                else
                {
                    if (middleDown)
                    {
                        middleEvent?.Invoke(ref game, gameTime, id, ButtonState.Released);
                    }
                    middleDown.Unset();
                }
            }
            else
            {
                rightDown = false;
                leftDown = false;
                middleDown = false;
                hoverEvent?.Invoke(ref game, gameTime, id, ButtonState.Released);
            }
        }
    }

    /// <summary>Represents copy with text location, message, and color.</summary>
    public class Text : DrawableGameComponent
    {
        public Vector2 location;
        public string message;
        public Color color;

        public Text(Vector2 location, string message, Color color, int drawOrder = 0) : base(game)
        {
            this.location = location;
            this.message = message;
            this.color = color;
            this.DrawOrder = drawOrder;
        }

        public override void Draw(GameTime gameTime)
        {
            spriteBatch.DrawString(game.Services.GetService<SpriteFont>(), message, new Vector2(location.X - camera.X, location.Y - camera.Y), color);
        }
    }
}
