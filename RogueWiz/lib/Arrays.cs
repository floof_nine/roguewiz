﻿using System;
using System.Linq;

namespace RogueWiz.lib
{
    public static class Arrays
    {
        public static T[,] ResizeArray<T>(T[,] original, int cols, int rows)
        {
            var newArray = new T[cols, rows];
            int minRows = Math.Min(cols, original.GetLength(0));
            int minCols = Math.Min(rows, original.GetLength(1));
            for (int i = 0; i < minRows; i++)
                for (int j = 0; j < minCols; j++)
                    newArray[i, j] = original[i, j];
            return newArray;
        }

        public static T[] GetRow<T>(this T[,] matrix, int rowNumber)
        {
            return Enumerable.Range(0, matrix.GetLength(0))
                    .Select(x => matrix[x, rowNumber])
                    .ToArray();
        }

        public static T[] GetCol<T>(this T[,] matrix, int columnNumber)
        {
            return Enumerable.Range(0, matrix.GetLength(1))
                    .Select(x => matrix[columnNumber, x])
                    .ToArray();
        }
    }
}
