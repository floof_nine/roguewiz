﻿using System;
using System.Collections.Generic;

namespace Hate9
{
    public static class IntuitiveRNG
    {
        public static double boostIncriment = 0.01;
        static Identification identification = new Identification();
        static Dictionary<int, double> IdBoosts = new Dictionary<int, double>();
        public static readonly Random realRNG = new Random();

        public static double randomNumber
        {
            get
            {
                return realRNG.NextDouble();
            }
        }

        public static bool RandomChance(double probability, int Id = -1)
        {
            Double number = randomNumber;
            probability = ExaggerateProbability(probability);
            if (Id != -1)
            {
                if (!IdBoosts.ContainsKey(Id))
                {
                    IdBoosts.Add(Id, 0);
                }
                identification.CreateId(Id);
            }
            probability += IdBoosts[Id];
            if (number <= probability)//if thing occurred
            {
                if (Id != -1)
                {
                    if (IdBoosts[Id] > 0)
                    {
                        IdBoosts[Id] = 0;
                    }
                    IdBoosts[Id] -= boostIncriment;
                }
                return true;
            }
            else//if thing did not occur
            {
                if (Id != -1)
                {
                    if (IdBoosts[Id] < 0)
                    {
                        IdBoosts[Id] = 0;
                    }
                    IdBoosts[Id] += boostIncriment;
                }
                return false;
            }
        }
        /*
        /// <summary>
        /// Receives a set of weighted possibilities and returns one of them at "random".
        /// </summary>
        /// <param name="possibilities">The ID of each possibility, and the chances of each possibility occurring. These values do not need to add up to 1.</param>
        public static Int32 RandomChance(params KeyValuePair<int, double>[] possibilities)
        {
            Dictionary<int, KeyValuePair<int, double>> probabilities = possibilities.ToDictionary(a => a.Key, a => new KeyValuePair<int, double>(identification.CreateId(), a.Value));//OH WAIT, IF I CREATE NEW IDS EVERY TIME, THEY'RE USELESS
            double sum = 0;
            foreach (KeyValuePair<int, double> kv in probabilities.Values)
            {
                sum += kv.Value;
            }
            Double number = randomNumber * sum;

            for (int i = 0; i < probabilities.Count; i++)
            {
                probabilities[i] = new KeyValuePair<int, double>(probabilities[i].Key, i >= 0 ? probabilities[i - 1].Value + probabilities[i].Value : probabilities[i].Value);
                if (!IdBoosts.ContainsKey(probabilities[i].Key))
                {
                    IdBoosts.Add(probabilities[i].Key, 0);
                }
                if (number <= probabilities[i].Value)
            }

            if (number <= newProbability)
            {
                if (Id != -1)
                {
                    if (identification.IdExists(Id))
                    {
                        if (IdBoosts[Id] > 0)
                        {
                            IdBoosts[Id] = 0;
                        }
                        IdBoosts[Id] -= boostIncriment;
                    }
                    else
                    {
                        Ids.Add(Id);
                        IdBoosts.Add(boostIncriment);
                    }
                }
                return true;
            }
            else
            {
                if (Id != -1)
                {
                    if (identification.IdExists(Id))
                    {
                        if (IdBoosts[Id] < 0)
                        {
                            IdBoosts[Id] = 0;
                        }
                        IdBoosts[Id] += boostIncriment;
                    }
                    else
                    {
                        Ids.Add(Id);
                        IdBoosts.Add(boostIncriment);
                    }
                }
                return false;
            }
        }*/

        public static int GenerateId()
        {
            int id = identification.CreateId();
            IdBoosts.Add(id, 0);
            return id;

        }

        public static double ExaggerateProbability(double probability, int iterations = 1, uint reductions = 0)
        {
            double oldProbability = probability;
            for (int i = 0; i < iterations; i++)
            {
                probability = 1 - Math.Cos(probability * Math.PI) * 0.5 - 0.5;
            }
            return (oldProbability * reductions + probability) / (reductions + 1.0);
        }
    }
}
