﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using RogueWiz.lib;
using System;
using System.Collections.Generic;
using System.Linq;
using static Hate9.IntuitiveRNG;
using static RogueWiz.lib.GameVars;
using static RogueWiz.lib.Maths;
using static RogueWiz.Methods;
using static RogueWiz.Program;

namespace RogueWiz
{
    static class Methods
    {
        /// <summary>
        /// Plays a song on a loop.
        /// </summary>
        /// <param name="songId">The index of the song to be played, within <see cref="music"/>.</param>
        public static void PlaySong(int songId)
        {
            if (songId != -1)
            {
                if (music.point != songId)
                {
                    music.point = songId;
                    return;
                }
                Timer temp = new Timer((float)music[songId].Duration.TotalSeconds, false, true, x => PlaySong((byte)music.point));
                game.Components.Add(temp);
                MediaPlayer.Play(music[music.point]);
            }
        }

        /// <summary>
        /// Gets the screen dimensions specified by the current scene
        /// </summary>
        public static Point GetScreenDimensions()
        {
            return scenes.GetValueAtPoint().screenDimensions;
        }

        /// <summary>
        /// Updates the <see cref="keyboardState"/>, <see cref="mouseState"/>, and <see cref="mousePosition"/> variables.
        /// </summary>
        public static void UpdateInputs()
        {
            keyboardState = Keyboard.GetState();
            mouseState = Mouse.GetState();

            mousePosition = new Point(mouseState.X * camera.Width / GetScreenDimensions().X, mouseState.Y * camera.Height / GetScreenDimensions().Y);
        }

        /// <summary>
        /// Shakes the camera back and fourth in a random direction
        /// </summary>
        /// <param name="delay">How long to wait before shaking the camera back</param>
        /// <param name="randomMultiplier">How many times to shake the camera randomly</param>
        /// <param name="regularMultiplier">How many times to shake the camera back and fourth per random shake</param>
        public static void RandomCameraShake(double delay = 0.05, int randomMultiplier = 2, int regularMultiplier = 1)
        {
            RandomCameraShake(randomNumber * 4 + 1, delay, randomMultiplier, regularMultiplier);
        }

        /// <summary>
        /// Shakes the camera back and fourth in a random direction
        /// </summary>
        /// <param name="distance">How far to shake the camera</param>
        /// <param name="delay">How long to wait before shaking the camera back</param>
        /// <param name="randomMultiplier">How many times to shake the camera randomly</param>
        /// <param name="regularMultiplier">How many times to shake the camera back and fourth per random shake</param>
        public static void RandomCameraShake(double distance, double delay = 0.05, int randomMultiplier = 2, int regularMultiplier = 1)
        {
            ShakeCamera(RandomRadian(), distance, delay, regularMultiplier);
            if (randomMultiplier > 1)
            {
                DelayCast(x => RandomCameraShake(distance, delay, randomMultiplier - 1), delay * regularMultiplier * 2);
            }
        }

        /// <summary>
        /// Shakes the <see cref="camera"/> back and forth by the specified amount in the specified direction the specified number of times.
        /// </summary>
        /// <param name="direction">The direction in which to shake the camera.</param>
        /// <param name="distance">The amount by which to move the camera.</param>
        /// <param name="multiplier">The number of times to shake the camera. Non-integer values are supported.</param>
        /// <param name="delay">The amount of time to wait between shakes.</param>
        public static void ShakeCamera(double direction, double distance, double delay = 0.05, int multiplier = 1)
        {
            if (delay < 0.05)
                delay = 0.05;

            DelayCast((args) => MoveCamera((double)args[0], (double)args[1]), delay, direction, distance);
            DelayCast((args) => MoveCamera((double)args[0], (double)args[1]), delay * 2, direction + Math.PI, distance);
            if (multiplier > 1)
            {
                DelayCast(x => ShakeCamera(direction, distance, delay, multiplier - 1), delay * 2);
            }
        }

        /// <summary>
        /// Moves the <see cref="camera"/> in the specified direcion by the specified amount.
        /// </summary>
        /// <param name="direction">The angle (in radians) at which to move the camera.</param>
        /// <param name="amount">The amount to move the camera.</param>
        public static void MoveCamera(double direction, double amount)
        {
            MoveCamera((int)Math.Round(amount * Math.Cos(direction)), (int)Math.Round(amount * Math.Sin(direction)));
        }

        /// <summary>
        /// Move the <see cref="camera"/> by the specified X and Y values.
        /// </summary>
        /// <param name="X">The amount by which the camera will be moved right.</param>
        /// <param name="Y">The amount by which the camera will be moved down.</param>
        public static void MoveCamera(int X, int Y)
        {
            camera.X += X;
            camera.Y += Y;
        }

        /// <summary>
        /// Runs the command after a specified amount of time.
        /// </summary>
        /// <param name="action">The command to be run</param>
        /// <param name="delay">The amount of time to wait before running the command, in seconds.</param>
        /// <param name="args">Any values you wish to pass to the command</param>
        public static void DelayCast(Timer.TimerEventHandler action, double delay, params object[] args)
        {
            Timer move = new Timer(delay, false, true, action, args);
            game.Components.Add(move);
        }
    }

    public static class Initializers
    {
        public static void InitializeVariables()
        {
            cursor = new ListP<Texture2D>();
            showCursor = true;
            variables = new Dictionary<int, object>();
            scenes = new ListP<Scene>();
            scenes.OnPointChanged += x => scenes[x].SwitchTo();
            music = new ListP<Song>();
            music.OnPointChanged += PlaySong;
            effects = new Dictionary<string, SoundEffect>();
            flags = new bool[65536];
            camera = new Rectangle(0, 0, Properties.Settings.Default.Dimensions.X, Properties.Settings.Default.Dimensions.Y);
            objectIden = new Hate9.Identification(1036286256);//random int
        }

        public static void SetupGraphics()
        {
            if (Properties.Settings.Default.Fullscreen && GraphicsAdapter.DefaultAdapter.CurrentDisplayMode.Width / (float)GraphicsAdapter.DefaultAdapter.CurrentDisplayMode.Height == Properties.Settings.Default.Dimensions.X / (float)Properties.Settings.Default.Dimensions.Y)
            {
                graphics.PreferredBackBufferWidth = GraphicsAdapter.DefaultAdapter.CurrentDisplayMode.Width;
                graphics.PreferredBackBufferHeight = GraphicsAdapter.DefaultAdapter.CurrentDisplayMode.Height;
                graphics.IsFullScreen = true;
            }
            else
            {
                graphics.PreferredBackBufferWidth = Properties.Settings.Default.Dimensions.X;
                graphics.PreferredBackBufferHeight = Properties.Settings.Default.Dimensions.Y;
            }
            graphics.ApplyChanges();
        }
    }

    public static class Extensions
    {
        /// <summary>
        /// Creates a <see cref="ListP{T}"/> from an array of type <see cref="T"/>.
        /// </summary>
        public static ListP<T> ToListP<T>(this T[] array)
        {
            List<T> tempList = array.ToList();
            ListP<T> returnListP = new ListP<T>();
            returnListP.AddRange(tempList);
            return returnListP;
        }

        /// <summary>
        /// Determines if an <see cref="object"/> can be converted to the specified type.
        /// </summary>
        /// <typeparam name="T">The specified type</typeparam>
        /// <param name="obj">The <see cref="object"/> to be tested</param>
        public static bool ConvertibleTo<T>(this object obj) where T : class
        {
            if (obj == null)
            {
                return true;
            }
            T temp = obj as T;
            return temp != null;
        }

        //Boolean extension methods

        /// <summary>Sets a boolean as true.</summary>
        public static void Set(this ref bool b)
        {
            b = true;
        }

        /// <summary>Sets a boolean as falaw.</summary>
        public static void Unset(this ref bool b)
        {
            b = false;
        }

        /// <summary>Sets a boolean as the opposite of its current value.</summary>
        public static void Invert(this ref bool b)
        {
            b = !b;
        }
    }
}
