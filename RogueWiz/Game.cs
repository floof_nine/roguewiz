﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using RogueWiz.lib;
using System.Collections.Generic;
using System.Linq;
using static Hate9.IntuitiveRNG;
using static RogueWiz.Initializers;
using static RogueWiz.lib.GameVars;
using static RogueWiz.Methods;

namespace RogueWiz
{
    /// <summary>This is the main type for your game.</summary>
    public class CurrentGame : Game
    {
        public int[,] map = new int[8, 8];
        public byte health = 3;
        public uint score = 0;
        public int spawnWait = 500;

        public CurrentGame()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
        }

        public void DrawSprite(Texture2D sprite, Point location)
        {
            DrawSprite(sprite, new Rectangle(location, sprite.Bounds.Size));
        }

        public void DrawSprite(Texture2D sprite, Rectangle bounds)
        {
            spriteBatch.Draw(sprite, bounds.Subtract(camera.Location), null, Color.White);
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            InitializeVariables();
            SetupGraphics();
            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            preRenderTarget = new RenderTarget2D(GraphicsDevice, camera.Width, camera.Height);
            spriteBatch = new SpriteBatch(GraphicsDevice);
            //REGULAR SETUP STARTS HERE

            Window.Title = "Rogue Wizard";

            cursor.Add(Content.Load<Texture2D>("cursor"));
            music.Add(Content.Load<Song>("gameBeat"));
            Services.AddService(Content.Load<SpriteFont>("xnorrunes"));

            effects.Add("step", Content.Load<SoundEffect>("step5-1"));
            effects.Add("hit", Content.Load<SoundEffect>("attack2"));
            effects.Add("die", Content.Load<SoundEffect>("death2"));
            effects.Add("kill", Content.Load<SoundEffect>("death"));
            effects.Add("cast", Content.Load<SoundEffect>("cast"));

            PlayerCharacter player = new PlayerCharacter(new SpriteSet(Content.Load<Texture2D>("character2")), new Point(3), new Point(3, 1));

            //The game scene
            scenes.Add(new Level(
                new HashSet<GameComponent>() {
                    new ImageDrawComponent(Content.Load<Texture2D>("bg"), new Point(-8), -10),

                    player,

                    new ImageDrawComponent(Content.Load<Texture2D>("vignette"), new Point(-8), 100),

                    new FunctionComponent(delegate(ref CurrentGame g, GameTime t, ref Dictionary<string, object> vars)
                        {
                            vars["tick"] = (int)vars["tick"] + 1;

                            if ((int)vars["tick"] >= spawnWait)
                            {
                                Point spawnSprite = new Point(realRNG.Next(0, 3), 0);
                                Point spawnLoc = Point.Zero;
                                if (realRNG.Next(2) == 1)
                                {
                                    spawnLoc.X = realRNG.Next(map.GetLength(0) - 1);
                                    spawnLoc.Y = realRNG.Next(2) * (map.GetLength(1) - 1);
                                }
                                else
                                {
                                    spawnLoc.X = realRNG.Next(2) * (map.GetLength(0) - 1);
                                    spawnLoc.Y = realRNG.Next(map.GetLength(1) - 1);
                                }
                                if (player.mapLocation != spawnLoc)
                                {
                                    Components.Add(new Goblin(spawnLoc, spawnSprite));
                                }

                                vars["tick"] = 0;
                                if (spawnWait > 100)
                                {
                                    spawnWait -= 50;
                                }
                            }
                        },
                        new Dictionary<string, object>() { { "tick", 500 } }
                    ),

                    new FunctionDrawComponent(componentDrawEvent: delegate(ref CurrentGame g, GameTime t, ref Dictionary<string, object> vars)
                    {
                        spriteBatch.DrawString(Services.GetService<SpriteFont>(), score.ToString(), new Vector2(2, -5), Color.White);
                    }, drawOrder: 101),

                    new FunctionDrawComponent(componentDrawEvent: delegate(ref CurrentGame g, GameTime t, ref Dictionary<string, object> vars)
                    {
                        Texture2D sprite = (Texture2D)vars["lifeSprite"];
                        for (int i = 1; i <= health; i++)
                        {
                            spriteBatch.Draw(sprite, new Rectangle(new Point(camera.Size.X - i * sprite.Width, 0), sprite.Bounds.Size), Color.White);
                        }
                    },
                    vars: new Dictionary<string, object>() { { "lifeSprite", Content.Load<Texture2D>("heart") } }, drawOrder: 101)

                }, new Rectangle(0, 0, 64, 64), 0
            ));

            //gameover
            scenes.Add(new Scene(Color.Black,
                new HashSet<GameComponent>()
                {
                    new Text(new Vector2(12, 12), "Game Over", Color.White),

                    new FunctionDrawComponent(delegate(ref CurrentGame g, GameTime t, ref Dictionary<string, object> vars)
                    {
                        if (Properties.Settings.Default.HighScore < score)
                        {
                            Properties.Settings.Default.HighScore = score;
                            Properties.Settings.Default.Save();
                        }
                    },
                    delegate(ref CurrentGame g, GameTime t, ref Dictionary<string, object> vars)
                    {
                        spriteBatch.DrawString(Services.GetService<SpriteFont>(), $"Score: {score}", new Vector2(12, 22), Color.White);
                    }),

                    new FunctionDrawComponent(componentDrawEvent: delegate(ref CurrentGame g, GameTime t, ref Dictionary<string, object> vars)
                    {
                        spriteBatch.DrawString(Services.GetService<SpriteFont>(), $"High: {Properties.Settings.Default.HighScore}", new Vector2(12, 32), Color.White);
                    }),

                    new Timer(3, true, timerTick: x => scenes.point = 2)
                },
                camera: new Rectangle(0, 0, 64, 64)
            ));


            //The main menu
            scenes.Add(new Scene(Color.Black, new HashSet<GameComponent>()
            {
                new Text(new Vector2(8, 5), "Rogue Wizard", Color.White),

                new Button(new SpriteSet(Content.Load<Texture2D>("playButton"), new Point(24, 9)), new Rectangle(20, 27, 24, 9),
                leftEvent: delegate(ref CurrentGame g, GameTime t, int id, ButtonState state)
                {
                    Button thisButton = (Button)g.Components.FirstOrDefault(x => x.ConvertibleTo<Button>() && ((Button)x).id == id);
                    if (thisButton != null)
                    {
                        if (state == ButtonState.Pressed)
                        {
                            thisButton.currentSprite.Y = 1;
                        }
                        else
                        {
                            thisButton.currentSprite.Y = 0;
                            scenes.point = 0;
                        }
                    }
                },
                hoverEvent: delegate(ref CurrentGame g, GameTime t, int id, ButtonState state)
                {
                    Button thisButton = (Button)g.Components.FirstOrDefault(x => x.ConvertibleTo<Button>() && ((Button)x).id == id);
                    if (thisButton != null)
                    {
                        if (state == ButtonState.Released)
                        {
                            thisButton.currentSprite.Y = 0;
                        }
                    }
                }),

                new Button(new SpriteSet(Content.Load<Texture2D>("aboutButton"), new Point(24, 9)), new Rectangle(4, 45, 24, 9),
                leftEvent: delegate(ref CurrentGame g, GameTime t, int id, ButtonState state)
                {
                    Button thisButton = (Button)g.Components.FirstOrDefault(x => x.ConvertibleTo<Button>() && ((Button)x).id == id);
                    if (thisButton != null)
                    {
                        if (state == ButtonState.Pressed)
                        {
                            thisButton.currentSprite.Y = 1;
                        }
                        else
                        {
                            thisButton.currentSprite.Y = 0;
                            scenes.point = 3;
                        }
                    }
                },
                hoverEvent: delegate(ref CurrentGame g, GameTime t, int id, ButtonState state)
                {
                    Button thisButton = (Button)g.Components.FirstOrDefault(x => x.ConvertibleTo<Button>() && ((Button)x).id == id);
                    if (thisButton != null)
                    {
                        if (state == ButtonState.Released)
                        {
                            thisButton.currentSprite.Y = 0;
                        }
                    }
                }),

                new Button(new SpriteSet(Content.Load<Texture2D>("helpButton"), new Point(24, 9)), new Rectangle(36, 45, 24, 9),
                leftEvent: delegate(ref CurrentGame g, GameTime t, int id, ButtonState state)
                {
                    Button thisButton = (Button)g.Components.FirstOrDefault(x => x.ConvertibleTo<Button>() && ((Button)x).id == id);
                    if (thisButton != null)
                    {
                        if (state == ButtonState.Pressed)
                        {
                            thisButton.currentSprite.Y = 1;
                        }
                        else
                        {
                            thisButton.currentSprite.Y = 0;
                            scenes.point = 4;
                        }
                    }
                },
                hoverEvent: delegate(ref CurrentGame g, GameTime t, int id, ButtonState state)
                {
                    Button thisButton = (Button)g.Components.FirstOrDefault(x => x.ConvertibleTo<Button>() && ((Button)x).id == id);
                    if (thisButton != null)
                    {
                        if (state == ButtonState.Released)
                        {
                            thisButton.currentSprite.Y = 0;
                        }
                    }
                })
            },
            camera: new Rectangle(0, 0, 64, 64)));


            //The about screen
            scenes.Add(new Scene(Color.Black, new HashSet<GameComponent>()
            {
                new Text(new Vector2(8, 5), "About", Color.White),

                new Button(new SpriteSet(Content.Load<Texture2D>("homeButton"), new Point(24, 9)), new Rectangle(33, 9, 24, 9),
                leftEvent: delegate(ref CurrentGame g, GameTime t, int id, ButtonState state)
                {
                    Button thisButton = (Button)g.Components.FirstOrDefault(x => x.ConvertibleTo<Button>() && ((Button)x).id == id);
                    if (thisButton != null)
                    {
                        if (state == ButtonState.Pressed)
                        {
                            thisButton.currentSprite.Y = 1;
                        }
                        else
                        {
                            thisButton.currentSprite.Y = 0;
                            scenes.point = 2;
                        }
                    }
                },
                hoverEvent: delegate(ref CurrentGame g, GameTime t, int id, ButtonState state)
                {
                    Button thisButton = (Button)g.Components.FirstOrDefault(x => x.ConvertibleTo<Button>() && ((Button)x).id == id);
                    if (thisButton != null)
                    {
                        if (state == ButtonState.Released)
                        {
                            thisButton.currentSprite.Y = 0;
                        }
                    }
                }),

                new Text(new Vector2(5, 24), "Game by:", Color.White),
                new Text(new Vector2(5, 31), "Aidan Schaefer", Color.White),
                new Text(new Vector2(5, 39), "Created for:", Color.White),
                new Text(new Vector2(5, 46), "PROG2370", Color.White),
            },
            camera: new Rectangle(0, 0, 64, 64)));


            //The help screen
            scenes.Add(new Scene(Color.Black, new HashSet<GameComponent>()
            {
                new Text(new Vector2(40, 5), "Help", Color.White),

                new Button(new SpriteSet(Content.Load<Texture2D>("homeButton"), new Point(24, 9)), new Rectangle(5, 9, 24, 9),
                leftEvent: delegate(ref CurrentGame g, GameTime t, int id, ButtonState state)
                {
                    Button thisButton = (Button)g.Components.FirstOrDefault(x => x.ConvertibleTo<Button>() && ((Button)x).id == id);
                    if (thisButton != null)
                    {
                        if (state == ButtonState.Pressed)
                        {
                            thisButton.currentSprite.Y = 1;
                        }
                        else
                        {
                            thisButton.currentSprite.Y = 0;
                            scenes.point = 2;
                        }
                    }
                },
                hoverEvent: delegate(ref CurrentGame g, GameTime t, int id, ButtonState state)
                {
                    Button thisButton = (Button)g.Components.FirstOrDefault(x => x.ConvertibleTo<Button>() && ((Button)x).id == id);
                    if (thisButton != null)
                    {
                        if (state == ButtonState.Released)
                        {
                            thisButton.currentSprite.Y = 0;
                        }
                    }
                }),

                new Text(new Vector2(5, 18), "Kill the goblins!", Color.White),
                new Text(new Vector2(5, 26), "Fire Rune:", Color.White),
                new Text(new Vector2(5, 33), "Space or Z", Color.White),
                new Text(new Vector2(5, 41), "Move:", Color.White),
                new Text(new Vector2(5, 48), "WASD or arrows", Color.White)
            },
            camera: new Rectangle(0, 0, 64, 64)));

            //set the starting scene to the main menu
            scenes.point = 2;
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            if (IsActive)
            {
                UpdateInputs();
            }
            base.Update(gameTime);
        }

        /// <summary>This is called when the game should draw itself.</summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.SetRenderTarget(preRenderTarget);
            GraphicsDevice.Clear(scenes.GetValueAtPoint().backgroundColor);

            spriteBatch.Begin(samplerState: SamplerState.PointClamp);

            base.Draw(gameTime);

            if (showCursor && GraphicsDevice.Viewport.Bounds.Contains(mousePosition))
            {
                DrawSprite(cursor[cursor.point], mousePosition);
            }
            spriteBatch.End();


            GraphicsDevice.SetRenderTarget(null);

            spriteBatch.Begin(samplerState: SamplerState.PointClamp);
            spriteBatch.Draw(preRenderTarget, GraphicsDevice.Viewport.Bounds, new Rectangle(Point.Zero, camera.Size), Color.White);
            spriteBatch.End();
        }
    }
}
