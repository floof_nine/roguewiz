﻿using System;

namespace RogueWiz
{
#if WINDOWS || LINUX
    /// <summary>The main class.</summary>
    public static class Program
    {
        public static CurrentGame game;
        public static string[] args = new string[0];
        /// <summary>The main entry point for the application.</summary>
        [STAThread]
        static void Main(string[] args)
        {
            Program.args = args;
            game = new CurrentGame();
            game.Run();
        }
    }
#endif
}
