# RogueWiz

## How to build
- Compile with the Visual Studio compiler
- Requires the .NET Core runtime installed
- Once the project is compiled, run the resulting .exe file to play



#### Note:

I have released this code without a permissive license, because I have created this repo for a school project, and otherwise had no intention to release the code.